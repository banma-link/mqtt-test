package com.github.bluesbruce.mqtt.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;

/**
 * Jackson工具类
 *
 * @author BBF
 */
public class JsonUtil {

  private final static ObjectMapper objectMapper = new ObjectMapper();

  /**
   * 将Java对象序列化成JSON字符串
   *
   * @param obj Java对象
   * @return JSON字符串
   */
  public static String toJson(final Map obj) {
    try {
      return objectMapper.writeValueAsString(obj);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "";
  }
}
