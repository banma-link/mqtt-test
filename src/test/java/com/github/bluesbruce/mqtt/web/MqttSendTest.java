package com.github.bluesbruce.mqtt.web;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MQTT消息发送测试类
 *
 * @author BBF
 */
public class MqttSendTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(MqttSendTest.class);
  private static final String TOPIC = "chat/general";

  @Test
  public void send() {
    MqttClientSender.publish(TOPIC, "hello,world", 0);
  }


  @Test
  public void sendThread() throws InterruptedException {
    int taskNum = 10000;
    long start_time = System.currentTimeMillis();
    LOGGER.info("Thread Run - Test 启动");
    final ExecutorService executorService = Executors.newFixedThreadPool(10);
    for (int i = 0; i < taskNum; i++) {
      final String txt = String.valueOf(i);
      executorService.execute(new Runnable() {
        @Override
        public void run() {
          MqttClientSender.publish(TOPIC, txt, 0);
        }
      });
    }
    executorService.shutdown();
    while (true) {
      if (executorService.isTerminated()) {
        LOGGER.info("[Run]最终耗时 = {}", System.currentTimeMillis() - start_time);
        break;
      }
      Thread.sleep(10);
    }
  }
}
