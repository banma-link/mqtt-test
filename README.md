# SpringBoot集成MQTT

## MQTT
``MQTT``（消息队列遥测传输）是ISO标准（``ISO/IEC PRF 20922``）下基于发布/订阅范式的消息协议。它工作在 ``TCP/IP``协议族上，是为硬件性能低下的远程设备以及网络状况糟糕的情况下而设计的发布/订阅型消息协议。国内很多企业都广泛使用``MQTT``作为``Android``手机客户端与服务器端推送消息的协议。

### 特点
``MQTT``协议是为大量计算能力有限，且工作在低带宽、不可靠的网络的远程传感器和控制设备通讯而设计的协议，它具有以下主要的几项特性：
1. 使用发布/订阅消息模式，提供一对多的消息发布，解除应用程序耦合；
2. 对负载内容屏蔽的消息传输；
3. 使用``TCP/IP``提供网络连接；
4. 有三种消息发布服务质量；
    1. **至多一次**：消息发布完全依赖底层 TCP/IP 网络。会发生消息丢失或重复。这一级别可用于如下情况，环境传感器数据，丢失一次读记录无所谓，因为不久后还会有第二次发送。
    2. **至少一次**：确保消息到达，但消息重复可能会发生。
    3. **只有一次**：确保消息到达一次。这一级别可用于如下情况，在计费系统中，消息重复或丢失会导致不正确的结果。
5. 小型传输，开销很小（固定长度的头部是 2 字节），协议交换最小化，以降低网络流量；
6. 使用``Last Will``和``Testament``特性通知有关各方客户端异常中断的机制。

## Apache-Apollo
``Apache Apollo``是一个代理服务器，其是在``ActiveMQ``基础上发展而来的，可以支持``STOMP``, ``AMQP``, ``MQTT``, ``Openwire``, ``SSL``, ``WebSockets`` 等多种协议。      
原理：服务器端创建一个唯一订阅号，发送者可以向这个订阅号中发东西，然后接受者（即订阅了这个订阅号的人）都会收到这个订阅号发出来的消息。以此来完成消息的推送。服务器其实是一个消息中转站。

### 下载

** 特别注意： Apollo已经不维护了，仅用作演示使用 **

下载地址：[http://archive.apache.org/dist/activemq/activemq-apollo/](http://archive.apache.org/dist/activemq/activemq-apollo/)

### 配置与启动

1. 需要安装JDK环境
2. 在命令行模式下进入``bin``，执行``apollo create mybroker d:\apache-apollo\broker``，创建一个名为``mybroker``虚拟主机（``Virtual Host``）。需要特别注意的是，生成的目录就是以后真正启动程序的位置。
3. 在命令行模式下进入``d:\apache-apollo\broker\bin``，执行``apollo-broker run``，也可以用``apollo-broker-service.exe``配置服务。
4. 访问``http://127.0.0.1:61680``打开web管理界面。（密码查看``d:\apache-apollo\mybroker\etc\users.properties``）
5. 启动端口，看``cmd``输出。

### Apache-Apollo界面
![UML](src/test/resources/p1.png)
![UML](src/test/resources/p2.png)
![UML](src/test/resources/p3.png)

### 单次消费的参数
![UML](src/test/resources/p4.png)

## 测试工具
在 ``resources/static/websocket``中，添加了``Apache-Apollo``自带的MQTT发送工具。    
访问``http://localhost:8081/``
